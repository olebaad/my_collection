﻿using System;
using System.Collections.Generic;

namespace My_Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            TestWithUserInput();
            Console.WriteLine("Hit enter to run automatic testing:");
            Console.ReadLine();
            AutoTest(20);
        }

        static void TestWithUserInput ()
        {
            bool validInt = false;
            int sizeOfChaos = 2;
            MyChaosArray<int> chaosArray = new MyChaosArray<int>(sizeOfChaos);
            bool stillRoomInCollection;

            do
            {
                int input;
                Console.Write("Please enter a number, or 'b' to go to next step: ");
                string userInput;

                do
                {
                    userInput = Console.ReadLine();
                    validInt = Int32.TryParse(userInput, out int entry);
                    input = entry;

                    if (userInput.ToLower() == "b")
                    {
                        break;
                    }

                    if (!validInt)
                    {
                        Console.Write("Please enter a valid Int32-number (or b): ");
                    }
                }
                while (!validInt);


                if (userInput.ToLower() == "b")
                {
                    break;
                }

                stillRoomInCollection = chaosArray.AddItem(input);

            }
            while (stillRoomInCollection);

            for (int i = 1; i <= sizeOfChaos/2; i++)
            {
                int value = chaosArray.GetItem();
                Console.WriteLine($"Randomly returned value no. {i} is:  {value}.");
            }
        }
        static void AutoTest (int sizeOfChaos)
        {
            MyChaosArray<int> chaosArray = new MyChaosArray<int>(sizeOfChaos);

            for (int number = 0; number < sizeOfChaos; number++)
            {
                chaosArray.AddItem(number);
            }

            for (int i = 1; i <= sizeOfChaos / 2; i++)
            {
                int value = chaosArray.GetItem();
                Console.WriteLine($"Randomly returned value no. {i} is:  {value}.");
            }
        }
    }

    class MyChaosArray<T>
    {
        private T[] TheMainList { get; set; }
        private int addedItems { get; set; }
        private int[] IndeciesInUse { get; set; }

        public MyChaosArray(int size = 100) 
        {
            TheMainList = new T[size];              //In example (T = int), this creates a list with all values = 0
            IndeciesInUse = new int[size];
            Array.Fill<int>(IndeciesInUse, -1);
        }

        public bool AddItem(T item)
        {
            if (addedItems >= TheMainList.Length)
            {
                Console.WriteLine("List is full, unable to comply :(");
                return false;
            } 
            else
            {
                Random rand = new Random();
                int safeGuard = 0;

                while (true)
                {
                    bool indexNotInUse;
                    int index;

                    do
                    {
                        index = rand.Next(0, TheMainList.Length);
                        indexNotInUse = Array.Exists(IndeciesInUse, elem => elem == index);
                        if (!indexNotInUse)
                        {
                            IndeciesInUse[addedItems] = index;
                            //foreach (int i in IndeciesInUse) { Console.Write($"{i}, "); }
                        }
                        //Console.WriteLine($"Index: {index} is checked is: {indexNotInUse}");
                    } 
                    while (indexNotInUse);

                    if (TheMainList[index] != null)
                    {
                        TheMainList[index] = item;
                        addedItems++;
                        Console.WriteLine($"Added Item in index {index}");
                        break;
                    }

                    if (safeGuard > TheMainList.Length + 100)
                    {
                        Console.WriteLine("Break at safeguard point, AddItem() method is broken.");
                        break;
                    }

                    safeGuard++;
                }

                return true;
            }
        }

        public T GetItem()
        {
            Random rand = new Random();
            int index = rand.Next(0, TheMainList.Length);

            return TheMainList[index];
        }
    }
}
